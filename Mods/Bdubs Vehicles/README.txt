Install the "Bdubs Vehicles" folder into your 7 days to Die "Mods" folder.

In order to see the correct item and vehicle names, you'll need to open the Localization.txt file included in this mod and copy and paste its contents into you game's Localization file. 

The default path for it is .../7 Days to Die/Data/Config/Localization.txt. Just copy it at the bottom and save it. 

*Note that any game updates will probably reset the file, so you'll have to repeat the process every time you update the game.